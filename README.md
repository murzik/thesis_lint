# Thesis linting for LaTeX documents

Writing a scientific paper often requires tedious checks, such as:

- Do all footnotes terminate with a dot?
- Are all external hyperlinks still valid?

The aim of this project is to automate the boring stuff to let you focus on the content. Suggestions and contributions are highly appreciated!

# Usage

```
$ git clone https://gitlab.com/murzik/thesis_lint.git
$ cd thesis_lint
$ cargo run LATEX_FILE
```

# Example 

```
$ cargo run error_examples/captions_without_trailing_dot.tex
Rule 'All captions have to end with a dot' was violated by:
  0: \caption{Some title}
  1: \caption[Short version]{A much longer title}
  2: \caption[Short version with dot.]{A much longer title without dot}
  3: \caption[Short version without dot]{A much longer title with dot.}
```

# Roadmap

- [x] Find footnotes without trailing dot
- [x] Find captions without trailing dot
- [x] Create a Dockerfile for continuous integration
- [ ] [Find invalid hyperlinks](#1)
- [ ] [Provide command-line options to turn rules on and off](#2)