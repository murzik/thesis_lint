FROM rust:slim AS builder
WORKDIR /work/
COPY Cargo.* ./
COPY src src
RUN cargo build --release

FROM debian:stretch-slim
COPY --from=builder /work/target/release/thesis_lint /usr/bin/
CMD ["sh"]
