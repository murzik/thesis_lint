extern crate regex;
extern crate structopt;
extern crate thesis_lint;

use structopt::StructOpt;
use thesis_lint::{run, Configuration};

fn main() {
    let config = Configuration::from_args();
    run(config);
}
