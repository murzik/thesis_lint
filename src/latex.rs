#![warn(clippy::all, clippy::pedantic)]
//! Provides methods to find and "validate" LaTeX commands.

use regex::Regex;
use regex::RegexSet;

/// Returns a slice of a LaTeX command like `\footnote{` including its closing `}`.
/// Supports arbitrary nesting. Does not support escaped brackets.
///
/// # Examples
///
/// Basic usage:
///
/// ```
/// use thesis_lint::latex::find_end;
///
/// let slice = find_end(r"\foobar{} foo bar baz").unwrap();
/// assert_eq!(r"\foobar{}", slice);
///
/// let slice = find_end(r"\foobar{{{}}} foo bar baz").unwrap();
/// assert_eq!(r"\foobar{{{}}}", slice);
/// ```
pub fn find_end(contents: &str) -> Option<&str> {
    let mut counter: u32 = 0;

    for (i, byte) in contents.bytes().enumerate() {
        match (byte, counter) {
            (b'{', _) => counter += 1,
            (b'}', 1) => return Some(&contents[..=i]),
            (b'}', _) => counter -= 1,
            (_, _) => (),
        }
    }

    None
}

fn get_elements_by_pattern<'a, 'b>(regex: &'a Regex, haystack: &'b str) -> Vec<&'b str> {
    let indices: Vec<&str> = regex
        .find_iter(haystack)
        .map(|m| m.start())
        .map(|start_byte| find_end(&haystack[start_byte..]))
        .filter_map(|o| o)
        .collect();

    let mut results: Vec<&str> = Vec::new();
    for &slice in &indices {
        results.push(slice);
    }

    results
}

lazy_static! {
    static ref RE_CAPTIONS: Regex = Regex::new(r"(\\caption(:?\{|\[))").unwrap();
    static ref RE_CAPTION_ENDS_WITH_DOT: RegexSet = RegexSet::new(&[
        r"^\\caption\[",
        r"^\\caption\[[\da-zA-Z\säöüÜÄÖ,.`-]+\.\]?",
        r"(\.\s*\}$)|(\.\s*\]\{[\da-zA-Z]+\}\s*\}\s*$)",
    ])
    .unwrap();
    static ref RE_FOOTNOTES: Regex = Regex::new(r"\\footnote\{").unwrap();
    static ref RE_FOOTNOTE_ENDS_WITH_DOT: Regex =
        Regex::new(r"(\.\s*\}$)|(\.\s*\]\{[\da-zA-ZöüäÖÜÄ'`_-]+\}\s*\}\s*$)").unwrap();
    static ref RE_HYPERLINKS: Regex = Regex::new(r"\\href\{").unwrap();
}

pub fn get_captions(contents: &str) -> Vec<&str> {
    get_elements_by_pattern(&RE_CAPTIONS, contents)
}

pub fn caption_ends_with_dot(caption: &str) -> bool {
    let matches: Vec<_> = RE_CAPTION_ENDS_WITH_DOT
        .matches(caption)
        .into_iter()
        .collect();
    matches == vec![2] || matches == vec![0, 1, 2]
}

pub fn get_footnotes(contents: &str) -> Vec<&str> {
    get_elements_by_pattern(&RE_FOOTNOTES, contents)
}

pub fn footnote_ends_with_dot(footnote: &str) -> bool {
    RE_FOOTNOTE_ENDS_WITH_DOT.captures(footnote).is_some()
}

pub fn get_hyperlinks(contents: &str) -> Vec<&str> {
    get_elements_by_pattern(&RE_HYPERLINKS, contents)
}

pub fn hyperlink_is_reachable(hyperlink: &str) -> bool {
    false //TODO
}

#[cfg(test)]
mod test {
    use crate::*;
    use std::fs::read_to_string;

    #[test]
    fn test_get_footnotes() {
        let contents = read_to_string("error_examples/footnotes_without_trailing_dot.tex").unwrap();
        let footnotes = latex::get_footnotes(&contents);

        assert_eq!(footnotes.len(), 2);
        assert_eq!(footnotes, vec![r"\footnote{Foo}", r"\footnote{Bar}"]);
    }

    fn assert_eq_batch<T>(cases: Vec<(bool, &'static str)>, check_valid: T)
    where
        T: Fn(&str) -> bool,
    {
        for (is_valid, case) in cases {
            assert_eq!(is_valid, check_valid(case), "{}", case);
        }
    }

    #[test]
    fn test_footnote_ends_with_dot() {
        let footnotes = vec![
            (true, r"\footnote{Foo.}"),
            (true, r"\footnote{Foo.     }"),
            (true, r"\footnote{\citealt[14f.]{Wolff2015}}"),
            (true, r"\footnote{\citealt[14f.]{Wolff2015}   }"),
            (true, r"\footnote{\citealt[14f. ]{Wolff2015}   }"),
            (true, r"\footnote{Jazz \href{bla}{bla}.}"),
            (false, r"\footnote{Foo}"),
            (false, r"\footnote{\citealt[14f]{Wolff2015}}"),
            (false, r"\footnote{Jazz \href{bla}{bla}}"),
        ];

        assert_eq_batch(footnotes, latex::footnote_ends_with_dot);
    }

    #[test]
    fn test_get_captions() {
        let contents = read_to_string("error_examples/captions_without_trailing_dot.tex").unwrap();
        let captions = latex::get_captions(&contents);

        assert_eq!(captions.len(), 4);
    }

    #[test]
    fn test_caption_ends_with_dot() {
        let captions = vec![
            (true, r"\caption{A title with dot.}"),
            (
                true,
                r"\caption[Short version with dot.]{A long title with dot.}",
            ),
            (false, r"\caption{A title without dot}"),
            (false, r"\caption[Short title]{A much longer title}"),
            (
                false,
                r"\caption[Short title with dot.]{A title without dot}",
            ),
            (
                false,
                r"\caption[Short title without dot]{A title with dot.}",
            ),
        ];

        assert_eq_batch(captions, latex::caption_ends_with_dot);
    }
}
