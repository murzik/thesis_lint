#![warn(clippy::all, clippy::pedantic)]

use crate::latex;
use std::process;

#[derive(Debug, PartialEq)]
struct Violation<'a> {
    element: &'a str,
}

struct Rule {
    /// What a rule should enforce
    description: &'static str,

    /// How to sieve text to get elements of interest
    filter_elements: Box<Fn(&str) -> Vec<&str>>,

    /// How to validate an element of interest
    is_valid: Box<Fn(&str) -> bool>,
}

impl Rule {
    /// Is the rule violated by any element of the `content`?
    fn check<'a, 'b>(&'a self, content: &'b str) -> Result<(), Vec<Violation<'b>>> {
        let violations: Vec<Violation> = (self.filter_elements)(content)
            .iter()
            .filter(|element| !(self.is_valid)(element))
            .map(|&f| Violation { element: f })
            .collect();

        match violations.len() {
            0 => Ok(()),
            _ => Err(violations),
        }
    }
}

pub fn validate(content: &str) {
    let rules = vec![
        Rule {
            description: "All footnotes have to end with a dot",
            filter_elements: Box::new(latex::get_footnotes),
            is_valid: Box::new(latex::footnote_ends_with_dot),
        },
        Rule {
            description: "All captions have to end with a dot",
            filter_elements: Box::new(latex::get_captions),
            is_valid: Box::new(latex::caption_ends_with_dot),
        },
    ];

    let mut errors = false;

    for rule in rules {
        match rule.check(content) {
            Ok(_) => (),
            Err(violations) => {
                errors = true;

                eprintln!("Rule '{}' was violated by:", rule.description);
                for (i, violation) in violations.iter().enumerate() {
                    eprintln!("  {}: {}", i, violation.element);
                }
            }
        };
    }

    if errors {
        process::exit(1);
    }
}

#[cfg(test)]
mod test {
    use crate::validation::{Rule, Violation};

    #[test]
    fn test_rule_check() {
        let rule = Rule {
            description: "only 'a', 'b', 'c'  allowed",
            filter_elements: Box::new(|_| vec!["a", "d", "c", "b", "e", "q"]),
            is_valid: Box::new(|s: &str| match s {
                "a" | "b" | "c" => true,
                _ => false,
            }),
        };

        // Notice that the passed `content` argument is ignored by the closure defined above
        let result = rule.check("");

        assert!(result.is_err());
        assert_eq!(
            result.err().unwrap(),
            vec!(
                Violation { element: "d" },
                Violation { element: "e" },
                Violation { element: "q" },
            )
        );
    }
}
