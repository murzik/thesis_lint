#![warn(clippy::all, clippy::pedantic)]

#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate structopt;

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use std::process;

use structopt::StructOpt;

pub mod latex;
pub mod validation;

#[derive(Debug, StructOpt)]
pub struct Configuration {
    /// Input file
    #[structopt(name = "FILE", parse(from_os_str))]
    path: PathBuf,
}

pub fn run(conf: Configuration) {
    let mut input = File::open(&conf.path).unwrap_or_else(|e| {
        eprintln!("Could not open file {:?}: {}", &conf.path, e);
        process::exit(1);
    });

    let contents = read_from_file(&mut input);
    validation::validate(&contents);
}

fn read_from_file(file: &mut File) -> String {
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    contents
}
